# MiaoywwwTools

### \- 这是一个来自Miaomiaoywww的小工具箱 -

---
## 功能
|   功能   | 是否实现 |
| :------: | -------- |
|  点名器  | √        |
|   壁纸   | √        |
| 其他功能 | 等待探索 |

---

## 如何安装?

+ 1.安装[.NET Desktop Runtime 6.0.3](https://download.visualstudio.microsoft.com/download/pr/33dd62b5-7676-483d-836c-e4cb178e3924/0de6894b5fdb6d130eccd57ab5af4cb8/windowsdesktop-runtime-6.0.3-win-x86.exe)

+ 2.下载最新的Relase版本: [MiaoywwwTools/Relase](https://github.com/Miaoywww/MiaoywwwTools/releases)

+ 3.解压到一个`英文`文件夹中,打开 MiaoywwwTools.exe 即可使用


# 感谢JetBrains对本项目的大力支持

[<img src="./jb_beam.png" alt="https://www.jetbrains.com/" title="JetBrains" width="40%">](https://www.jetbrains.com/)


**特别鸣谢:**

- [Hitokoto / 一言](https://hitokoto.cn/)

- [Handycontrol](http://handyorg.gitee.io/)
 